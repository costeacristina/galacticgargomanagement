package ship;

public class Ship {
	
private String  name;
private double travel;
private double time;

public Ship()
{
}

public Ship(String  name, double travel, double time)
{
	this.name = name;
	this.travel = travel;
	this.time = time;
}

public String getName() 
{
	return name;
}

public void setName(String name)
{
	this.name = name;
}

public double getTravel() 
{
	return travel;
}

public void setTravel(double travel)
{
	this.travel = travel;
}

public double getTime() 
{
	return time;
}

public void setTime(double time)
{
	this.time = time;
}

}
