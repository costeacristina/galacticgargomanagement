package mainApplication;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.InputMismatchException;
import java.util.Scanner;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import ship.Ship;

public class Main {

public static void main(String[] args) 
{
	ArrayList<Ship> ships = new ArrayList<>();
	Scanner scanner = new Scanner(System.in);
	String character;
	long weight = 0;
	String destination;
	
	System.out.println("Enter character you want use");
	character = scanner.nextLine();
    System.out.println("Enter the cargo weight");
    try {
    weight=scanner.nextLong();
    }
    catch (InputMismatchException e)
    {
    	System.out.println("Error! You must enter a weight as a long");
    	System.out.println();
    	System.out.println("Bye!");
    	System.exit(-1);
    }
    System.out.println("Enter destination planet");
    scanner.nextLine();
    destination=scanner.nextLine();
    
    //read from json
    JSONParser parser = new JSONParser();
    JSONArray characterJson = new JSONArray();
    JSONArray shipJson = new JSONArray();
    JSONArray planetJson = new JSONArray();
    try 
    {
    	characterJson = (JSONArray) parser.parse(new FileReader("C:\\Users\\Cris\\Desktop\\New folder\\GalacticCargoManagement\\files\\characters.json"));
    	shipJson = (JSONArray) parser.parse(new FileReader("C:\\Users\\Cris\\Desktop\\New folder\\GalacticCargoManagement\\files\\ships.json"));
    	planetJson = (JSONArray) parser.parse(new FileReader("C:\\Users\\Cris\\Desktop\\New folder\\GalacticCargoManagement\\files\\planets.json"));
	} 
    catch (FileNotFoundException e) 
    {
    	e.printStackTrace();
    }
    catch (IOException e) 
    {
    	e.printStackTrace();
    } 
    catch (ParseException e) 
    {
    	e.printStackTrace();
    }

	for (Object characterObject : characterJson)
	{
		JSONObject characterRead = (JSONObject) characterObject;
		String name = (String) characterRead.get("name");
		if (name.equals(character))
		{
			ArrayList<String> shipsType = (ArrayList<String>)characterRead.get("shipsType");
			for (int i = 0; i<shipsType.size(); i++)
			{
				for (Object shipObject : shipJson)
				{
					JSONObject shipRead = (JSONObject) shipObject;
					String nameShip = (String) shipRead.get("type");
					if (nameShip.equals(shipsType.get(i)))
					{
						for (Object planetObject : planetJson)
						{
							JSONObject planetRead = (JSONObject) planetObject;
							String namePlanet = (String) planetRead.get("name");
							if (namePlanet.equals(destination))
							{
								long distance = (long) planetRead.get("distance");
								if ((long)shipRead.get("maxCargoWeight") >= weight)
								{
									Ship s = new Ship();
									s.setName((String) shipRead.get("name"));
									s.setTime((float)distance / (long)shipRead.get("speed"));
									s.setTravel(1);
									ships.add(s);
								}
								else
								{
									float trips = (float)weight / (long)shipRead.get("maxCargoWeight");
									Ship s = new Ship();
									s.setName((String) shipRead.get("name"));
									s.setTime((Math.round(trips) + Math.round((trips-1))) * (float)distance / (long)shipRead.get("speed"));
									s.setTravel(trips);
									ships.add(s);
								}
							}
						}
					}
				}
			}
		}	
	}
	
	if (ships.size() == 0)
	{
		System.out.println("No results for given information! Please give another information");
		System.out.println();
		main(null);
	}
	
	System.out.println("The results is:");
	Collections.sort(ships, Comparator.comparingDouble(Ship ::getTime));
	for (int i = 0; i<ships.size(); i++)
	{
		System.out.println(character + ", can transport a cargo of "+ weight+ "KG to "+destination+ ", in "+ ships.get(i).getTime()+" hours, using the "+ ships.get(i).getName()+ " in "+ Math.round(ships.get(i).getTravel()) +" trips");
	}
	
	System.out.println();
	System.out.println("If you want to try again, please press the 'y' key, followed by 'enter' key. Otherwise, press another key");
	char key = scanner.next().charAt(0);
	if (key == 'y')
	{
		main(null);
	}
	else
	{
		System.out.println();
		System.out.println("Bye!");
		System.exit(0);
	}
}

}
